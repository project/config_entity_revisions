<?php

namespace Drupal\webform_revisions;

use Drupal\webform\WebformSubmissionListBuilder;

class WebformRevisionsSubmissionListBuilder extends WebformSubmissionListBuilder{
  use WebformRevisionsSubmissionListBuilderTrait;
}
