<?php

namespace Drupal\webform_revisions;

use Drupal\webform_workflows_element\WebformSubmissionWorkflowListBuilder;

class WebformRevisionsSubmissionWorkflowListBuilder extends WebformSubmissionWorkflowListBuilder {
  use WebformRevisionsSubmissionListBuilderTrait;
}
