<?php

namespace Drupal\webform_revisions;

use Drupal\webform\WebformSubmissionStorageInterface;

trait WebformRevisionsSubmissionListBuilderTrait {

  /**
   * {@inheritdoc}
   */
  protected function initialize() {
    parent::initialize();

    $storage = $this->getStorage();

    $ids = $this->getEntityIds();
    $submissions = $storage->loadMultiple($ids);

    $webforms = [];
    $columns = [];
    foreach ($submissions as $submission) {
      $webform = $submission->getWebform();
      $webformId = $webform->id();
      $webformRevisionId = $webform->getRevisionId();
      if (empty($webforms[$webformId])) {
        $webforms[$webformId] = [];
      }
      if ($webforms[$webformId][$webformRevisionId]) {
        continue;
      }

      $webforms[$webformId][$webformRevisionId] = true;
      $submissionColumns = $storage->getCustomColumns($submission->getWebform());
      $columns = array_merge($columns, $submissionColumns);
    }

    $this->columns = $columns;
  }

}
