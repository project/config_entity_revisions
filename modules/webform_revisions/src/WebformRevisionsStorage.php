<?php

namespace Drupal\webform_revisions;

use Drupal\config_entity_revisions\ConfigEntityRevisionsStorageTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\webform\WebformEntityStorage;
use Drupal\webform_revisions\Controller\WebformRevisionsController;

class WebformRevisionsStorage extends WebformEntityStorage {
  use ConfigEntityRevisionsStorageTrait;

  /**
   * Load a revision of a webform.
   *
   * @param $revision_id
   *   The revision to be loaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  function loadRevision($revision_id) {
    return $this->_loadRevision($revision_id);
  }

}
