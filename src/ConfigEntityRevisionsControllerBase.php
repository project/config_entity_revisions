<?php

namespace Drupal\config_entity_revisions;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\diff\DiffEntityComparison;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\Serializer\Serializer;
use Drupal\Core\Database\Connection;

/**
 * Controller to make library functions available to various consumers.
 */
abstract class ConfigEntityRevisionsControllerBase extends ControllerBase implements ConfigEntityRevisionsControllerInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Wrapper object for simple configuration from diff.settings.yml.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Wrapper object for simple configuration from diff.settings.yml.
   *
   * @var \Drupal\diff\DiffEntityComparison;
   */
  protected $entityComparison;

  /**
   * Serialiser service.
   *
   * @var Serializer;
   */
  protected $serialiser;

  /**
   * Container instance.
   *
   * @var ContainerInterface
   */
  protected $container;

  /**
   * Date formatter service.
   *
   * @var DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The database connection.
   *
   * @var Connection
   */
  protected $connection;

  /**
   * The Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected  $moduleHandler;

  /**
   * Constructs a ConfigEntityRevisionsController object.
   *
   * @param ContainerInterface $container
   *   The container interface object.
   * @param DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param RendererInterface $renderer
   *   The renderer service.
   * @param ImmutableConfig $config
   *   The configuration service.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param AccountProxyInterface $current_user
   *   The current user.
   * @param Serializer $serialiser
   *   The serialiser service.
   * @param Connection $connection
   *   The database connection.
   * @param ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param DiffEntityComparison $entity_comparison | NULL
   *   The diff entity comparison service.
   */
  public function __construct(
    ContainerInterface $container,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    ImmutableConfig $config,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxyInterface $current_user,
    Serializer $serialiser,
    Connection $connection,
    ModuleHandlerInterface $moduleHandler,
    DiffEntityComparison $entity_comparison = NULL
  ) {
    $this->container = $container;
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->config = $config;
    $this->entityComparison = $entity_comparison;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->serialiser = $serialiser;
    $this->connection = $connection;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_comparison = $container->has('diff.entity_comparison') ?
      $container->get('diff.entity_comparison') : NULL;

    return new static(
      $container,
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('config.factory')->get('diff.settings'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('serializer'),
      $container->get('database'),
      $container->get('module_handler'),
      $entity_comparison
    );
  }

  /**
   * Create revision when a new config entity version is saved.
   *
   * @param ConfigEntityRevisionsInterface $configEntity
   *   The configuration entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createUpdateRevision(ConfigEntityRevisionsInterface $configEntity) {

    if ($configEntity->get('content_entity_save')) {
      return;
    }

    /* @var $contentEntity \Drupal\config_entity_revisions\ConfigEntityRevisionsEntityInterface */
    $contentEntity = NULL;
    $revisionPublished = FALSE; // Default is unpublished.
    $revisionWasPublished = FALSE;

    $alreadyDoingRevisionUpdate = !empty($configEntity->revisionUpdate) ? $configEntity->revisionUpdate : FALSE;
    unset($configEntity->revisionUpdate);

    $contentID = $configEntity->getContentEntityID();
    $currentRevision = $configEntity->getRevisionId();

    $newRevision = $contentEntityNeedsSave = !$alreadyDoingRevisionUpdate &&
      (!$contentID || $configEntity->get('revision'));

    if (!$contentID) {
      $source = $configEntity->original ?? $configEntity;

      /* @var $contentEntity ContentEntityInterface */
      $contentEntity = $configEntity->contentEntityStorage()->create([
        'configuration' => $this->serialiser->serialize($source, 'json'),
        'type' => $configEntity->getEntityTypeId() . "_revisions",
        'revision_log_message' => $this->t('Initial revision')
      ]);

      $revisionPublished = $source->status();
    }
    else {

      if (!empty($currentRevision)) {
        $contentEntity = $configEntity->contentEntityStorage()
          ->loadRevision($currentRevision);
      }

      if (!$contentEntity) {
        $contentEntity = $configEntity->contentEntityStorage()
          ->load($contentID);
      }

      if ($newRevision) {
        $contentEntity = $configEntity->contentEntityStorage()
          ->createRevision($contentEntity);
      }

      $serialised = $this->serialiser->serialize($configEntity, 'json');
      if ($serialised !== $contentEntity->get('configuration')) {
        $contentEntity->set('configuration', $serialised);
        $contentEntityNeedsSave = TRUE;
      }
      $revisionWasPublished = $contentEntity->isPublished();

      $message = $configEntity->get('revision_log_message');
      $new_message = $message ? $message[0]['value'] : '';

      if (!$alreadyDoingRevisionUpdate && !empty($new_message)) {
        if ($contentEntity->getRevisionLogMessage() != $new_message) {
          $contentEntity->setRevisionLogMessage($new_message);
          $contentEntityNeedsSave = TRUE;
        }
      }
    }

    if ($this->currentUser->id() != $contentEntity->getRevisionUserId()) {
      $contentEntity->setRevisionUserId($this->currentUser->id());
      $contentEntityNeedsSave = TRUE;
    }

    if ($this->moduleHandler->moduleExists('content_moderation')) {
      /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_info */
      $moderation_info = \Drupal::service('content_moderation.moderation_information');
      $moderation_enabled = $moderation_info->shouldModerateEntitiesOfBundle($contentEntity->getEntityType(), $contentEntity->bundle());
    }
    else {
      $moderation_enabled = FALSE;
    }
    if ($moderation_enabled) {
      $contentEntityModerationState = $revisionPublished ? 'published' : 'draft';

      if (!is_null($configEntity->get('moderation_state'))) {
        $contentEntityModerationState = $configEntity->get('moderation_state')[0]['value'];
      }

      if ($contentEntity->moderation_state->value != $contentEntityModerationState) {
        $contentEntity->moderation_state->value = $contentEntityModerationState;
        $contentEntityNeedsSave = TRUE;
      }
    }

    $wasDefaultRevision = $contentEntity->isDefaultRevision();

    $newPublishState =
      ((is_string($revisionPublished) && $revisionPublished == 'published') ||
        (!is_string($revisionPublished) && !$revisionPublished));

    if ($newPublishState != $contentEntity->isPublished())  {
      if ($revisionPublished) {
        $contentEntity->setPublished();
      }
      else {
        $contentEntity->setUnpublished();
      }

      $contentEntity->isDefaultRevision($revisionPublished);
      $contentEntityNeedsSave = TRUE;
    }

    if ($contentEntityNeedsSave) {
      $contentEntity->setRevisionCreationTime($this->container->get('datetime.time')
        ->getRequestTime());
      $contentEntity->save();
    }

    // Load a fresh, fully saved version.
    if ($contentEntity->id() !== $configEntity->getContentEntityID() ||
      $contentEntity->getRevisionId() !== $configEntity->getThirdPartySetting('webform_revisions', 'revisionId')) {
      $configEntity = $configEntity->configEntityStorage()
        ->load($configEntity->id());
      $configEntity->setContentEntityID($contentEntity->id());
      $configEntity->setThirdPartySetting('webform_revisions', 'revisionId', $contentEntity->getRevisionId());
      $configEntity->revisionUpdate = TRUE;
      $configEntity->save();
    }

    if (!$newPublishState && $revisionWasPublished && $wasDefaultRevision) {
      // Modify another revision to be published and default if possible.
      $this->resetDefaultRevision($contentEntity);
    }

    if ($newRevision) {
      $arguments = [$configEntity, $contentEntity];
      $this->moduleHandler()->invokeAll($configEntity->config_entity_name() . '_revision_insert', $arguments);
      $this->moduleHandler()->invokeAll('revision_insert', $arguments);
    }

    return $contentEntity;
  }

  /**
   * Make default the most recently published revision or the most recent
   * revision.
   *
   * This is needed because content_moderation has a concept of a default
   * revision, which this module doesn't really care about, but which will
   * cause problems if we attempt to delete a revision that's marked as the
   * default.
   *
   * @param ContentEntityInterface $content_entity
   *   The content (revisions) entity.
   */
  public function resetDefaultRevision(ContentEntityInterface $content_entity) {
    $content_entity_id = $content_entity->id();

    $revisions = $this->connection
      ->select("config_entity_revisions_revision", 'c')
      ->fields('c', ['revision', 'revision_default', 'published'])
      ->condition('id', $content_entity_id)
      ->orderBy('revision', 'DESC')
      ->execute()
      ->fetchAllAssoc('revision');

    $first_published = NULL;
    $first_revision = NULL;
    $remove_default = [];

    foreach ($revisions as $revision) {
      if (!$first_revision) {
        $first_revision = $revision;
      }

      if ($revision->published && !$first_published) {
        $first_published = $revision;
      }

      if ($revision->revision_default) {
        $remove_default[$revision->revision] = 1;
      }
    }

    $default_revision = $first_published ?: $first_revision;

    if ($default_revision) {
      unset($remove_default[$default_revision->revision]);
    }

    if (!empty($remove_default)) {
      $this->connection->update("config_entity_revisions_revision")
        ->condition('revision', array_keys($remove_default), 'IN')
        ->fields(['revision_default' => 0])
        ->execute();
    }

    if ($default_revision) {
      if (!$default_revision->revision_default) {
        $this->connection->update("config_entity_revisions_revision")
          ->condition('revision', $default_revision->revision)
          ->fields(['revision_default' => 1])
          ->execute();
      }

      $this->connection->update("config_entity_revisions")
        ->condition('id', $content_entity_id)
        ->fields(['revision' => $default_revision->revision])
        ->execute();
    }

  }

  /**
   * Get a list of revision IDs for a content entity.
   */
  public function getRevisionIds($content_entity_id) {
    $revisions = $this->connection->select("config_entity_revisions_revision", 'c')
      ->fields('c', ['revision'])
      ->condition('id', $content_entity_id)
      ->orderBy('id')
      ->execute()
      ->fetchCol();
    return $revisions;
  }

  /**
   * Delete a single revision.
   *
   * @param ContentEntityInterface $revision
   *   The revision to be deleted.
   */
  public function deleteRevision($revision) {
    $was_default = $revision->isDefaultRevision();

    // Let other modules delete other content that might be related.
    $this->moduleHandler->invokeAll('config_entity_revision_delete_related_content', [$revision]);

    if ($this->config_entity->has_own_content()) {
      $this->config_entity->deleteRelatedContentEntities($revision);
    }

    $revisions = $this->getRevisionIds($revision->id());

    if ($was_default) {
      // Change the default to the next newer (if we're deleting the default,
      // there must be no published revisions so it doesn't matter which we
      // choose. Ensure revision_default isn't set on our revision in
      // config_entity_revisions_revision - $was_default can return FALSE
      // even when that value is 1, and that will cause the content moderation
      // module (which does look at that field) to throw an exception.
      $this->connection->update("config_entity_revisions_revision")
        ->condition('id', $revision->id())
        ->fields(['revision_default' => 0])
        ->execute();
      $revision_to_use = ($revisions[0] == $this->revision->getRevisionId()) ?
        $revisions[1] : $revisions[0];
      $new_default = $this->configEntityRevisionsStorage->loadRevision($revision_to_use);
      $new_default->enforceIsNew(FALSE);
      $new_default->isDefaultRevision(TRUE);
      $new_default->save();
    }

    $this->entityTypeManager
      ->getStorage('config_entity_revisions')
      ->deleteRevision($revision->getRevisionId());
  }

  /**
   * Delete revisions when a config entity is deleted.
   *
   * @param ConfigEntityRevisionsInterface $configEntity
   *   The configuration entity being deleted.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteRevisions(ConfigEntityRevisionsInterface $configEntity) {

    $contentEntity = $configEntity->getContentEntity();

    if ($contentEntity) {
      $configEntity->contentEntityStorage()->delete([$contentEntity]);
    }
  }

  /**
   * Load a particular revision of a config entity.
   *
   * @param int $revision
   *   The revision ID to load.
   * @param mixed $entity
   *   The entity type to load.
   *
   * @return mixed
   *   The loaded revision or NULL.
   */
  public function loadConfigEntityRevision($revision = NULL, $entity = '') {
    $configEntityName = $this->config_entity_name();
    $storage = $this->entityTypeManager->getStorage($configEntityName);

    if (!$entity) {
        try {
          $match = \Drupal::service('router')->matchRequest(\Drupal::request());
        }
        catch(\Exception $exception) {
          $match = [];
      }
      $entity = isset($match[$configEntityName]) ? $match[$configEntityName]: NULL;
    }

    if ($revision) {
      $revisionsEntity = $this->entityTypeManager->getStorage('config_entity_revisions')
        ->loadRevision($revision);

      if ($revisionsEntity) {
        $entity = \Drupal::getContainer()->get('serializer')->deserialize(
          $revisionsEntity->get('configuration')->value, $storage->getEntityClass(),
          'json');

        $entity->set('enforceIsNew', FALSE);
        $loaded = [ $entity ];
        $entity::postLoad($storage, $loaded);
      }
    }

    if (is_string($entity)) {
      $entity = $storage->load($entity);
    }

    return $entity;
  }

}
