<?php


namespace Drupal\config_entity_revisions;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Url;

trait ConfigEntityRevisionsConfigTrait {

  /**
   * Restore the entity type manager after deserialisation.
   */
  public function __wakeup() {
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
  }

  /**
   * Gets the revision identifier of the entity.
   *
   * @return int
   *   The revision identifier of the entity, or NULL if the entity does not
   *   have a revision identifier.
   */
  public function getRevisionId() {
    return $this->getThirdPartySetting($this->module_name(), 'revisionId');
  }

  /**
   * Set revision ID.
   *
   * @param int $revisionID
   *   The revision ID that this class instance represents.
   */
  public function setRevisionId($revisionID) {
    $this->setThirdPartySetting($this->module_name(), 'revisionId', $revisionID);
  }

  /**
   * Get the config entity storage.
   *
   * @return ConfigEntityStorageInterface
   *   The storage for the config entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function configEntityStorage() {
    return $this->entityTypeManager->getStorage($this->config_entity_name());
  }

  /**
   * Get the revisions entity storage.
   *
   * @return ContentEntityStorageInterface
   *   The storage for the revisions entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function contentEntityStorage() {
    return $this->entityTypeManager->getStorage('config_entity_revisions');
  }

  /**
   * Default revision of revisions entity that matches the config entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The matching entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getContentEntity() {
    $contentEntityID = $this->getContentEntityID();

    if (!$contentEntityID) {
      return NULL;
    }

    /* @var $storage \Drupal\Core\Entity\ContentEntityStorageInterface */
    $storage = $this->contentEntityStorage();

    // Get the matching revision ID if one is provided.
    if ($this->getRevisionId()) {
      return $storage->loadRevision($this->getRevisionId());
    }

    // Otherwise, just get the default revision.
    return $storage->load($contentEntityID);
  }

  /**
   * Get the number of revisions for this config entity.
   */
  public function getNumberOfRevisions() {
    $contentEntityID = $this->getContentEntityID();

    if (!$contentEntityID) {
      return 0;
    }

    /* @var $storage \Drupal\Core\Entity\ContentEntityStorageInterface */
    $revisions = $this->contentEntityStorage()->getQuery()
      ->condition($this->getEntityType()->getKey('id'), $contentEntityID)
      ->allRevisions()
      ->accessCheck(FALSE)
      ->execute();
    return count($revisions);
  }

  /**
   * Link to this revision.
   */
  public function revisionLink($text) {
    $contentEntity = $this->getContentEntity();
    if ($this->has_canonical_url() && $contentEntity->isDefaultRevision() && $contentEntity->isPublished()) {
      $link = $this->toLink($text)->toString();
    }
    else {
      $configEntityType = $this->getEntityType()->id();
      $url = new Url("entity.{$configEntityType}.revision", [
        $configEntityType => $this->id(),
        "revision_id" => $this->getRevisionId(),
      ]);
      $link = \Drupal::linkGenerator()->generate($text, $url);
    }

    return $link;
  }

}
